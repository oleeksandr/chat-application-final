import { Document } from "mongoose";
import { IMessage } from "./IMessage";

export interface IMongoMessage extends IMessage, Document {
}