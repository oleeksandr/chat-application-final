import * as express from 'express';
import * as mongoose from 'mongoose';
import { Server } from 'http';
import * as socket from 'socket.io';
import * as path from 'path';
import * as Message from './models/Message';
import { IMongoMessage } from './interfaces/IMongoMessage';
import { IMessage } from './interfaces/IMessage';
const app = express();

const http = new Server(app);
const io = socket(http);
mongoose.connect('mongodb://localhost/messages', <mongoose.ConnectionOptions>{ useNewUrlParser: true });


app.get('/', (req: express.Request, res: express.Response) => res.send(`App is running. Port: ${port}. Use a Client to connect.`));

io.on('connection', async (socket: any) => {

    await Message.find({})
        .sort({ _id: -1 })
        // .limit(10)
        .then((message: IMongoMessage[]) => {
            socket.emit('chat history', message.reverse().map((elem: IMongoMessage) => {
                return { author: elem.author, message: elem.message,}
            }));
        });

    socket.on('user joined the room', (userName: string) => {
        socket.broadcast.emit('system message', {author:"System", message:userName + " joined the room"});
    });

    socket.on('chat message', (msg: IMessage) => {
        socket.broadcast.emit('chat message', msg);
        const newMessage = new Message(msg);
        newMessage.save();
    });

    socket.on('user is typing', (userName: string) => {
        socket.broadcast.emit('user is typing', userName);
    });
});

const port = 3005;
http.listen(port, () => console.log(`App listening on port ${port}!`));