import * as mongoose from 'mongoose';
import { IMongoMessage } from '../interfaces/IMongoMessage';

const messagesSchema = new mongoose.Schema(
    {
        author: { type: String, required: true },
        message: { type: String, required: true }
    }, {
        timestamps: true
    }
);

const Message = mongoose.model<IMongoMessage>("Message", messagesSchema);

export = Message;