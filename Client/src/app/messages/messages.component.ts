import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'Messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  constructor() { }

  @Input() public messages;

  ngOnInit() {
  }
}
