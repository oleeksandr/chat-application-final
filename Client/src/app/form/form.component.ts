import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ChatForm',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  @Output() sendMessageEvent = new EventEmitter<string>();
  @Output() sendUserTypingEvent = new EventEmitter<string>();
  message: string = "";

  constructor() { }

  ngOnInit() {
  }

  sendMessage() {
    if (this.message === '')
      return false;
    this.sendMessageEvent.next(this.message);
    this.message = '';
  }

  userIsTyping(event: KeyboardEvent) {
  if (event.keyCode !== 13)
      this.sendUserTypingEvent.next();
  }

}
