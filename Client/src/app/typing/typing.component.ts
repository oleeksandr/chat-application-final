import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { MapType } from '@angular/compiler';

interface UsersTyping {
  timerId: number,
  userName: string
}

@Component({
  selector: 'Typing',
  templateUrl: './typing.component.html',
  styleUrls: ['./typing.component.scss']
})

export class TypingComponent implements OnInit {

  typingUsers: UsersTyping[] = [];
  eventsSubscription: any;

  constructor() { }

  @Input() eventUserTyping: Observable<string>;
  @Input() eventNewMessageFromUser: Observable<string>;

  ngOnInit() {
    this.eventsSubscription = this.eventUserTyping.subscribe((userName: string) => this.addUserTyping(userName));
    this.eventsSubscription = this.eventNewMessageFromUser.subscribe((userName: string) => this.clearUserTyping(userName));
  }

  ngOnDestroy() {
    this.eventsSubscription.unsubscribe();
  }

  private addUserTyping = (userName: string) => {
    const findedIndex: number = this.typingUsers.findIndex((typingUser) => typingUser.userName === userName);
    if (findedIndex === -1) {
      //If started typing
      const timerId: number = window.setTimeout(() => {
        this.clearUserTyping(userName);
      }, 5000);
      this.typingUsers.push({ timerId: timerId, userName: userName });
    } else {
      //If continue typing

      //Clear old timer
      window.clearTimeout(this.typingUsers[findedIndex].timerId);

      //Set new one
      const timerId: number = window.setTimeout(() => {
        this.clearUserTyping(userName);
      }, 5000);

      //Write back to object
      this.typingUsers[findedIndex] = { timerId: timerId, userName: userName };
    }
  }

  private clearUserTyping = (userName: string) => {
    const findedIndex: number = this.typingUsers.findIndex((typingUser) => typingUser.userName === userName);

    if (findedIndex !== -1) {
      window.clearTimeout(this.typingUsers[findedIndex].timerId);
      this.typingUsers = this.typingUsers.filter((value, index) => index != findedIndex);
    }
  }

}
