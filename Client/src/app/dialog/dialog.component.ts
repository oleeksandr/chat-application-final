import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Validators, FormControl } from '@angular/forms';

export interface DialogData {
  userName: string;
}

@Component({
  templateUrl: './dialog.component.html'
})

export class DialogComponent {

  userNameFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) { }

}