import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { ChatService } from './chat.service';
import { IMessage } from './interfaces/IMessage';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material';
import { DialogComponent } from './dialog/dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  title: string = "Chat Application";
  messages: IMessage[] = [];
  userName: string = "";
  private userTypingEvents: Subject<string> = new Subject<string>();
  private newMessageFromUserEvent: Subject<string> = new Subject<string>();
  container: HTMLElement;
  chatInited: boolean = false;

  constructor(private chatService: ChatService, public dialog: MatDialog) { }

  @ViewChild('chat', { static: true }) chat: ElementRef;

  ngAfterViewInit() {
    this.scrollToTheEnd();
  }

  ngAfterViewChecked() {
    this.scrollToTheEnd();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      disableClose: true,
      data: { userName: this.userName }
    });

    dialogRef.afterClosed().subscribe((userName: string) => {
      this.userName = userName;
      this.chatService.sendUserJoinedTheRoom(this.userName);
    });
  }

  sendMessage(message: string) {
    this.chatService.sendMessage({ author: this.userName, message: message });
    this.messages.push({ author: "You", message: message });
  }

  sendUserTyping() {
    this.chatService.sendUserTyping(this.userName);
  }

  scrollToTheEnd() {
    if ((!this.chatInited && this.chat.nativeElement.scrollHeight > window.pageYOffset + window.innerHeight) ||
      (this.chatInited && this.chat.nativeElement.scrollHeight - 300 < window.pageYOffset + window.innerHeight)) {
      this.chatInited = true;
      window.scrollTo({
        'left': 0,
        'top': this.chat.nativeElement.scrollHeight
      });
    }
  }

  ngOnInit() {

    this.openDialog();

    //Event listeners
    this.chatService
      .getChatHistory()
      .subscribe((messages: IMessage[]) => {
        this.messages.push(...messages);
      });

    this.chatService
      .getUserThatTyping()
      .subscribe((userName: string) => {
        this.userTypingEvents.next(userName);
      });

    this.chatService
      .getMessages()
      .subscribe((message: IMessage) => {
        // User end typing
        this.newMessageFromUserEvent.next(message.author);
        this.messages.push(message);
      });

    this.chatService
      .getSystemMessage()
      .subscribe((systemMessage: IMessage) => {
        this.messages.push(systemMessage);
      });
  }
}
