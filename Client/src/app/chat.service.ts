import * as io from 'socket.io-client';
import { Observable } from 'rxjs';
import { IMessage } from './interfaces/IMessage';

export class ChatService {
    private url = 'http://localhost:3005';
    private socket;

    constructor() {
        this.socket = io(this.url);
    }

    public sendUserJoinedTheRoom(userName: string) {
        this.socket.emit('user joined the room', userName);
    }

    public sendUserTyping(userName: string) {
        this.socket.emit('user is typing', userName);
    }

    public sendMessage(message: IMessage) {
        this.socket.emit('chat message', message);
    }

    public getMessages = () => {
        return Observable.create((observer) => {
            this.socket.on('chat message', (message: IMessage) => {
                observer.next(message);
            });
        });
    }

    public getSystemMessage = () => {
        return Observable.create((observer) => {
            this.socket.on('system message', (userName: string) => {
                observer.next(userName);
            });
        });
    }

    public getUserThatTyping = () => {
        return Observable.create((observer) => {
            this.socket.on('user is typing', (userName: string) => {
                observer.next(userName);
            });
        });
    }

    public getChatHistory = () => {
        return Observable.create((observer) => {
            this.socket.on('chat history', (messages: IMessage[]) => {
                observer.next(messages);
            });
        });
    }
}